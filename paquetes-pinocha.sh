#!/bin/bash
# Script de istalación de los paquetes necesarios
# para pinocha. Correr con el usuario root

# Instalación de gephi:
# Mi referencia: 

echo '
Script de instalación de paquetes necesarios para Pinocha
Iniciando....
'
sleep 5

echo '
Descargando e instalando gephi...
'
cd /opt/
wget https://github.com/gephi/gephi/releases/download/v0.9.7/gephi-0.9.7-linux-x64.tar.gz
tar xzvf gephi-0.9.7-linux-x64.tar.gz
mv gephi-0.9.7 gephi
# Esto lo hago con ansible, pero lo dejo por si a alguien le sirve 
# echo 'Instalando dependencias del gephi'
# apt update && apt install libcanberra-gtk-module libcanberra-gtk3-module openjdk-11-jre
# Agrego lanzador para poner en favoritos
echo '
#!/usr/bin/env xdg-open
[Desktop Entry]
Version=1.0
Type=Application
Terminal=false
Exec=/opt/gephi/bin/gephi --jdkhome /usr/lib/jvm/java-11-openjdk-amd64/jre
Name=Gephi
Comment=Gephi
' > /usr/share/applications/Gephi.desktop

echo 'Todo pronto, verifica que todo haya salido bien'
echo 'Chau!'
exit 0